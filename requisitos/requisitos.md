# Requisitos e organização do projeto

## Lista de Requisitos
1) Por meio de uma página Web do seu sistema, um usuário deve ser capaz de fornecer (A) uma lista de partidos políticos que ele gostaria de monitorar e (B) uma lista de deputados federais que ele gostaria de monitorar. 

2) O sistema deve permitir remover e listar partidos e deputados, sempre de forma intuitiva e visualmente agradável. 

3) O usuário deve então ser capaz de inserir um conjunto de palavras-chave ou expressões (i.e., sequências de palavras separadas por espaço) indicando tópicos que ele gostaria de monitorar. Por exemplo "Meio-ambiente", "Saúde da mulher", "Educação infantil" ou "Ciência". 

4) Dado um período definido pelo usuário, o sistema deve carregar e mostrar de forma bonita e bem organizada um resumo de todos os votos das entidades selecionadas (partidos ou deputados). Esse resumo deve ser mostrado tanto em forma de texto, quanto em forma visual em gráficos. 

5) O sistema deve montar e mostrar de forma bonita e bem organizada os votos de acordo com as temáticas indicadas no item 3). 

6) Ao definir as palavras-chave e expressões, o usuário deve poder usar expressões regulares 

7) De uma forma bonita e elegante, o sistema deve mostrar duas nuvens de palavras distintas  representando os votos SIM e NÃO das entidades selecionadas (partidos ou deputados). O sistema deve também mostrar uma nuvem de palavras representando os projeto de lei propostos por cada entidade selecionada.

9) Opcional (ponto bônus): Sugira uma nova funcionalidade ainda não especificada acima. Ou então, incorpore no sistema um relato dos gastos de um partido ou deputados de acordo com  https://www2.camara.leg.br/transparencia/receitas-e-despesas/despesas/pesquisa-de-despesas.

### histórias de Usuário

As historias de usuário desenvolvidas para esse projeto estão disponíveis nesta [planilha do Drive](https://docs.google.com/spreadsheets/d/1Xlp9b2aNq4_lbjv93tyFeyQ0dwx_mTl8_Hmw1vPebwE/edit#gid=0)

## Mapa de Requisitos
Para organizar e discutir os requisitos, criamos um mapa de requisitos para entender com uma visão de épicos, historias de usuários e features do projeto.

[Mapa de Requisitos](https://mm.tt/2027301175?t=AX8lXi8pXY)

## EAP - Estrutura Analitica do Projeto


A estrutura Analítica do Projeto visa auxiliar na organização temporal com os marcos de entrega do projeto, alinhando as espectativas e prioridades com o contexto da disciplina.

[EAP (Estrutura Analitica do Projeto) on MindMeister](https://mm.tt/2010742116?t=1SgimJJ8kg)

