# Documentação

A documentação visa apresentar e armazenar toda a documentação referente à disciplina e o projeto que será desenvolvido. 

## Contexto

Tópicos Avançados em Programação Orientada a Objetos

Esse projeto foi desenvolvido no contexto da disciplina **MAC413/5714 - Tópicos Avançados de Programação Orientada a Objetos** que visa discutir conceitos avançados de modelagem e desenvolvimento do software orientado a objetos. Deste modo, é trabalhado os aspectos de desenvolvimento buscando identificar padrões arquiteturais que melhor atendam as necessidades dos requisitos. Isso inclui atividades de concepção, especificação e de desenvolvimento de um software, que vai desde a interface do usuário até os algoritmos e estruturas de dados necessários para implementar o software.

Dentre as opções o tema escolhido foi o **Monitor Politico**, que visa monitorar partidos e deputados em relação à proposições, votações e transparência das informações da forma mais simples e visualmente agradavel.

## Requisitos

Os requisitos e a Estrutura Analitica do Projeto do projeto estão disponíveis no [Documento de Requisitos](requisitos/requisitos.md).

## Arquitetura do Software

A arquitetura de Software, no contexto desse projeto, é a definição dos elementos arquiteturais, como banco de dados, servidores, clientes, entre outros. Envolve também como será a interação entre esses elementos, quais padrões serão necessários para o projeto e quais as restrições desses padrões.

Essas especificações e mais detalhes sobre o projeto estão disponíveis no nosso [Documento de arquitetura](arquitetura/docArquitetura.md).


## Contribuir
Você quer contribuir com nosso projeto? É simples! Temos um [guia de contribuição](docs/CONTRIBUTING.md) onde são explicados todos os passos para contribuir. Ahh, não esquece de ler nosso [código de conduta](docs/CODE_OF_CONDUCT.md).   
Caso reste duvidas você também pode entrar em contato conosco criando uma issue.  
Para contribuir com os nossos projetos acesse nossa organização: [MonitorPolitico](https://gitlab.com/poo-monitor-poltico).








<!-- Nosso projeto está utilizando a seguinte arquitetura para funcionar:

![ilustração da arquitetura](img/arq.drawio.png)

Os dados dos partidos são extraídos e convertidos em um [container no DockerHub](https://hub.docker.com/r/everaldogjr/postgre) manualmente, o [back-end](https://gitlab.com/poo-monitor-poltico/backend) e o [front-end](https://gitlab.com/poo-monitor-poltico/frontend) estão localizados aqui no gitlab e cada um possui a própria configuração docker-compose, que se conecta com todos os container e permite que o projeto inteiro rode a partir de qualquer um dos repositórios. -->
