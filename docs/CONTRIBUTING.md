# Guia de Contribuição

## Como contribuir?

Para contribuir com o projeto é muito fácil e cada pouquinho conta! Basta seguir os seguintes passos:

* *Fork* do repositório (apenas para usuários externos)
* Criar [*branchs*](CONTRIBUTING.md#política-de-branches)
* Submeter [*Pull Request*](CONTRIBUTING.md#política-de-merges-e-pull-requests)


### Política de Branches

#### *main*

A branch *main* é a branch de produção, onde ficará a versão estável do projeto. Ela estará bloqueada para commits e para pushs.

#### *dev*

A branch *dev* é a branch de desenvolvimento, onde o trabalho das outras branchs será unificado e onde será criada uma versão estável para mesclar com a *main*.
Assim como a *main* ela está bloqueada para commits e pushs.


### Política de Merges e Pull Requests

#### Pull Requests

Pull requests devem ser feitos para a branch *main* seguindo as regras e os passos do tópico [*Merges para dev*](CONTRIBUTING.md#merges-para-dev). No conteúdo do pull request deve haver uma descrição clara do que foi feito.

Deve ser seguido o [template Pull Request](prTemplate.md).

##### Work in Progress

Caso haja a necessidade de atualizar a branch *main* antes de concluir a issue, o nome do pull request deve conter WIP:<X_nome_da_branch> para que a branch não seja deletada.

#### Merges para *dev*
Os merges para *dev* deverão ser feitos quando a funcionalidade ou refatoração estiverem de acordo com os seguintes aspectos:
- Funcionalidade ou refatoração concluída;
- Funcionalidade revisada por algum outro membro.

Para fazer um merge para *dev* os passos a serem seguidos são:
- `git checkout branch_de_trabalho`;
- `git pull --rebase origin dev`;
- `git push origin branch_de_trabalho`;
- Abrir pull request via interface GitLab;
- Aguardar Code Review


##### Code Review
O code review deve ser feito por um ou mais membros da equipe que não participaram das modificações.
Após pelo menos uma aprovação de Code Review, Status Check o PullRequest poderá ser aceito;

Para aceitar o PullRequest, deve-se usar a opção *merge* no Gitlab.
