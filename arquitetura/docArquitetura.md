# Documento de Arquitetura

## Representação da Arquitetura

### Representação das Relações

[<img src="diagramas/representacao_relacoes.png" width="1300" title="">]()

O diagrama apresenta cada etapa que será seguido para que o projeto Monitor Politico funcione. Relacionando o frontend com o Backend.


#### Iteração react e API

A aplicação web será desenvolvida utilizando a biblioteca JavaScript React.js. Essa biblioteca oferece componentização e reuso, com isso será reduzida a duplicação de código. Além disso, essa biblioteca é single page application, ou seja a interface interagem com o usuário dinamicamente reescrevendo a mesma página, sem carregar toda a página a medida que ela sofre alterações.

O React também tem boa performance porque mantém uma cópia do DOM (Document Object Model) em memória chamada Virtual-DOM. Quando um dado é alterado e é necessária uma alteração na view, o React verifica pelo Virtual-DOM quais partes devem ser alteradas. Dessa forma, evita a lentidão de acessar diretamente o DOM todo o tempo.


## Metas Restrições de Arquitetura

Para o desenvolvimento deste projeto serão ultilizadas as seguintes tecnologias e restrições:

- [React](https://pt-br.reactjs.org) Uma biblioteca JavaScript para criar interfaces de usuário
- [Java](https://www.java.com/pt-BR/) Linguagem utilizada para desenvolver a API
- [SpringBoot](https://spring.io/projects/spring-boot) Framework utilizado para o desenvolvimento da API
- [Docker](https://www.docker.com) Utilizado para conteinerização do ambiente de desenvolvimento

## Visão de Implementação

### Modelagem de dados (Conceitual)

[<img src="modelagem_banco/modelo_conceitual.png" width="1400" title="">]()

### Modelagem de dados (Lógico)

[<img src="modelagem_banco/modelo_logico.png" width="1400" title="">]()

### Diagrama de Pacotes

Diagrama de pacotes Backend (versão - 01)
[<img src="diagramas/pacotes_backend.png" width="1400" title="">]()

Diagrama de pacotes Frontend (versão - 01)
[<img src="diagramas/pacotes_front.png" width="1400" title="">]()
